/*
 * Copyright (c) 2014 to Adam van der Kruk (Zehavi) AKA TacB0sS - Nu-Art Software
 * Licensed under the Binpress license;
 */
package com.nu.art.software.pdf.core;


import com.sun.pdfview.core.PDFObject;
import com.sun.pdfview.core.PDFParseException;


public interface Decoder {
	
	public PDF_BufferWrapper decode(PDF_BufferWrapper buf, PDFObject params)
			throws PDFParseException;
}
