package com.nu.art.software.pdf.sdk.core.fonts;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sun.pdfview.core.PDFObject;
import com.sun.pdfview.core.PDFParseException;
import com.sun.pdfview.core.fonts.FontSupport;


public class PDFFontEncoding {

	private static final String TYPE0 = "Type0";

	private static final int TYPE_ENCODING = 0;

	private static final int TYPE_CMAP = 1;

	private int[] baseEncoding;

	private Map<Character, String> differences;

	private PDFCMap cmap;

	private int type;

	public PDFFontEncoding(String fontType, PDFObject encoding)
			throws PDFParseException {
		if (encoding.isName()) {
			// if the encoding is a String, it is the name of an encoding
			// or the name of a CMap, depending on the type of the font
			if (fontType.equals(TYPE0)) {
				type = TYPE_CMAP;
				cmap = PDFCMap.getCMap(encoding.getStringValue());
			} else {
				type = TYPE_ENCODING;

				differences = new HashMap<Character, String>();
				baseEncoding = this.getBaseEncoding(encoding.getStringValue());
			}
		} else {
			// loook at the "Type" entry of the encoding to determine the type
			String typeStr = null;
			PDFObject dictRef = encoding.getDictRef("Type");
			if (dictRef != null)
				typeStr = dictRef.getStringValue();

			if (typeStr == null || typeStr.equals("Encoding")) {
				// it is an encoding
				type = TYPE_ENCODING;
				parseEncoding(encoding);
			} else if (typeStr.equals("CMap")) {
				// it is a CMap
				type = TYPE_CMAP;
				cmap = PDFCMap.getCMap(encoding);
			} else {
				throw new IllegalArgumentException("Uknown encoding type: " + type);
			}
		}
	}

	/** Get the glyphs associated with a given String */
	public List<PDFGlyph> getGlyphs(PDFFont font, String text) {
		List<PDFGlyph> outList = new ArrayList<PDFGlyph>(text.length());

		// go character by character through the text
		char[] arry = text.toCharArray();
		for (int i = 0; i < arry.length; i++) {
			switch (type) {
				case TYPE_ENCODING :
					outList.add(getGlyphFromEncoding(font, arry[i]));
					break;
				case TYPE_CMAP :
					// 2 bytes -> 1 character in a CMap
					char c = (char) ((arry[i] & 0xff) << 8);
					if (i < arry.length - 1) {
						c |= (char) (arry[++i] & 0xff);
					}
					outList.add(getGlyphFromCMap(font, c));
					break;
			}
		}

		return outList;
	}

	/**
	 * Get a glyph from an encoding, given a font and character
	 */
	private PDFGlyph getGlyphFromEncoding(PDFFont font, char src) {
		String charName = null;

		// only deal with one byte of source
		src &= 0xff;

		// see if this character is in the differences list
		if (differences.containsKey(Character.valueOf(src))) {
			charName = differences.get(Character.valueOf(src));
		} else if (baseEncoding != null) {
			// get the character name from the base encoding
			int charID = baseEncoding[src];
			charName = FontSupport.getName(charID);
		}

		return font.getCachedGlyph(src, charName);
	}

	/**
	 * Get a glyph from a CMap, given a Type0 font and a character
	 */
	private PDFGlyph getGlyphFromCMap(PDFFont font, char src) {
		int fontID = cmap.getFontID(src);
		char charID = cmap.map(src);

		// if (font instanceof Type0Font) {
		// font = ((Type0Font) font).getDescendantFont(fontID);
		// }

		return font.getCachedGlyph(charID, null);
	}

	/**
	 * Parse a PDF encoding object for the actual encoding
	 */
	public void parseEncoding(PDFObject encoding)
			throws PDFParseException {
		differences = new HashMap<Character, String>();

		// figure out the base encoding, if one exists
		PDFObject baseEncObj = encoding.getDictRef("BaseEncoding");
		if (baseEncObj != null) {
			baseEncoding = getBaseEncoding(baseEncObj.getStringValue());
		}

		// parse the differences array
		PDFObject diffArrayObj = encoding.getDictRef("Differences");
		if (diffArrayObj != null) {
			PDFObject[] diffArray = diffArrayObj.getArray();
			int curPosition = -1;

			for (int i = 0; i < diffArray.length; i++) {
				if (diffArray[i].isNumber()) {
					curPosition = diffArray[i].getIntValue();
				} else if (diffArray[i].isName()) {
					Character key = Character.valueOf((char) curPosition);
					differences.put(key, diffArray[i].getStringValue());
					curPosition++;
				} else {
					throw new IllegalArgumentException("Unexpected type in diff array: " + diffArray[i]);
				}
			}
		}
	}

	/** Get the base encoding for a given name */
	private int[] getBaseEncoding(String encodingName) {
		if (encodingName.equals("MacRomanEncoding")) {
			return FontSupport.macRomanEncoding;
		} else if (encodingName.equals("MacExpertEncoding")) {
			return FontSupport.type1CExpertCharset;
		} else if (encodingName.equals("WinAnsiEncoding")) {
			return FontSupport.winAnsiEncoding;
		} else {
			throw new IllegalArgumentException("Unknown encoding: " + encodingName);
		}
	}
}
