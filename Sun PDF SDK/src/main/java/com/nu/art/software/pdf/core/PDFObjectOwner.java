/*
 * Copyright since 2014 belongs to: 'Adam van der Kruk (Zehavi)' AKA 'TacB0sS' and/or 'Nu-Art Software' Israel
 * ------
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

package com.nu.art.software.pdf.core;


import com.sun.pdfview.core.PDFDecrypter;
import com.sun.pdfview.core.PDFObject;
import com.sun.pdfview.core.PDFParseException;
import com.sun.pdfview.core.PDFXref;


public interface PDFObjectOwner {

	PDFDecrypter getDefaultDecrypter();

	PDFObject dereferenceOld(PDFXref value, PDFDecrypter decrypter)
			throws PDFParseException;

	PDF_BufferWrapper decodeStream(PDFObject pdfObject, PDF_BufferWrapper stream)
			throws PDFParseException;

	PDF_BufferWrapper createBuffer(byte[] bytes);

	Object getVersion();

}
