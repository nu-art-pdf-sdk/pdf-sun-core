package com.sun.pdfview.decoders;


import android.graphics.Matrix;

import com.nu.art.software.pdf.core.PDF_BufferWrapper;


public class _DecoderTools {

	/**
	 * Char Null
	 */
	private static final int Char_Null = 0;

	/**
	 * Horizontal Tab - <b>HT</b>
	 */
	private static final int Char_Tab = '\t';

	/**
	 * Line Feed - <b>LF</b>
	 */
	private static final int Char_LineFeed = '\n';

	/**
	 * Carriage Return - <b>CR</b>
	 */
	private static final int Char_CarriageReturn = '\r';

	/**
	 * Space - <b>Space</b>
	 */
	private static final int Char_Space = ' ';

	/**
	 * Form Feed - <b>FF</b>
	 */
	private static final int Char_FormFeed = 12;

	public static final int readHexPair(PDF_BufferWrapper buf) {
		int first = readHexDigit(buf);
		if (first < 0) {
			buf.position(buf.position() - 1);
			return -1;
		}
		int second = readHexDigit(buf);
		if (second < 0) {
			/*
			 * PDF32000_2008 - 7.3.4.3
			 */
			// buf.position(buf.position() - 1);
			return (first << 4);
		}
		return (first << 4) + second;
	}

	public static boolean isWhiteSpace(int c) {
		switch (c) {
			case Char_Null : // Null (NULL)
			case Char_Tab : // Horizontal Tab (HT)
			case Char_LineFeed : // Line Feed (LF)
			case Char_FormFeed : // Form Feed (FF)
			case Char_CarriageReturn : // Carriage Return (CR)
			case Char_Space : // Space (SP)
				return true;
			default :
				return false;
		}
	}

	public static final int readHexDigit(PDF_BufferWrapper buf) {
		int a;
		while (isWhiteSpace(a = buf.get())) {}
		return readHexDigit(a);
	}

	private static int readHexDigit(int a) {
		switch (a) {
			case '0' :
			case '1' :
			case '2' :
			case '3' :
			case '4' :
			case '5' :
			case '6' :
			case '7' :
			case '8' :
			case '9' :
				return a - '0';
			case 'a' :
			case 'b' :
			case 'c' :
			case 'd' :
			case 'e' :
			case 'f' :
				return a - 'a' + 10;
			case 'A' :
			case 'B' :
			case 'C' :
			case 'D' :
			case 'E' :
			case 'F' :
				return a - 'A' + 10;
			default :
				return -1;
		}
	}

	public static Matrix setMatValues(Matrix mat, float... values) {
		mat.setValues(new float[]{values[0], values[2], values[4], values[1], values[3], values[5], 0, 0, 1});
		return mat;
	}

	public static Matrix createMatrix(float... values) {
		return setMatValues(new Matrix(), values);
	}

	public static boolean isDelimiter(int c) {
		switch (c) {
			case '(' : // LEFT PARENTHESIS
			case ')' : // RIGHT PARENTHESIS
			case '<' : // LESS-THAN-SIGN
			case '>' : // GREATER-THAN-SIGN
			case '[' : // LEFT SQUARE BRACKET
			case ']' : // RIGHT SQUARE BRACKET
			case '{' : // LEFT CURLY BRACKET
			case '}' : // RIGHT CURLY BRACKET
			case '/' : // SOLIDUS
			case '%' : // PERCENT SIGN
				return true;
			default :
				return false;
		}
	}

	public static boolean isRegularCharacter(int c) {
		return !(isWhiteSpace(c) || isDelimiter(c));
	}

}
