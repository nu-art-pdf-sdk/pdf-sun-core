/*
 * $Id: PDFObject.java,v 1.8 2009/03/15 20:47:38 tomoke Exp $
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
package com.sun.pdfview.core;


import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import com.nu.art.software.pdf.core.PDFObjectOwner;
import com.nu.art.software.pdf.core.PDF_BufferWrapper;


/**
 * a class encapsulating all the possibilities of content for an object in a PDF file.
 * <p>
 * A PDF object can be a simple type, like a Boolean, a Number, a String, or the Null value. It can also be a NAME, which looks like a string, but is a special
 * type in PDF files, like "/Name".
 * <p>
 * A PDF object can also be complex types, including Array; Dictionary; Stream, which is a Dictionary plus an array of bytes; or Indirect, which is a reference
 * to some other PDF object. Indirect references will always be dereferenced by the time any data is returned from one of the methods in this class.
 *
 * @author Mike Wessler
 */
public class PDFObject {

	/** an indirect reference */
	public static final int INDIRECT = 0; // PDFXref

	/** a Boolean */
	public static final int BOOLEAN = 1; // Boolean

	/** a Number, represented as a double */
	public static final int NUMBER = 2; // Double

	/** a String */
	public static final int STRING = 3; // String

	/** a special string, seen in PDF files as /Name */
	public static final int NAME = 4; // String

	/** an array of PDFObjects */
	public static final int ARRAY = 5; // Array of PDFObject

	/** a Hashmap that maps String names to PDFObjects */
	public static final int DICTIONARY = 6; // HashMap(String->PDFObject)

	/** a Stream: a Hashmap with a byte array */
	public static final int STREAM = 7; // HashMap + byte[]

	/** the NULL object (there is only one) */
	public static final int NULL = 8; // null

	/** a special PDF bare word, like R, obj, true, false, etc */
	public static final int KEYWORD = 9; // String

	/**
	 * When a value of {@link #getObjGen objNum} or {@link #getObjGen objGen}, indicates that the object is not top-level, and is embedded in another object
	 */
	public static final int OBJ_NUM_EMBEDDED = -2;

	/**
	 * When a value of {@link #getObjGen objNum} or {@link #getObjGen objGen}, indicates that the object is not top-level, and is embedded directly in the
	 * trailer.
	 */
	public static final int OBJ_NUM_TRAILER = -1;

	/** the NULL PDFObject */
	public static final PDFObject nullObj = new PDFObject(null, NULL, null);

	/** the type of this object */
	private int type;

	/** the value of this object. It can be a wide number of things, defined by type */
	private Object value;

	/** the encoded stream, if this is a STREAM object */
	private PDF_BufferWrapper stream;

	public final PDF_BufferWrapper __TempGetSTREAM() {
		return stream;
	}

	/** a cached version of the decoded stream */
	private PDF_BufferWrapper decodedStream;

	/**
	 * the PDFFile from which this object came, used for dereferences
	 */
	private PDFObjectOwner owner;

	/**
	 * a cache of translated data. This data can be garbage collected at any time, after which it will have to be rebuilt.
	 */
	private Object cache;

	/** @see #getObjNum() */
	private int objNum = OBJ_NUM_EMBEDDED;

	/** @see #getObjGen() */
	private int objGen = OBJ_NUM_EMBEDDED;

	/**
	 * create a new simple PDFObject with a type and a value
	 *
	 * @param owner the PDFFile in which this object resides, used for dereferencing. This may be null.
	 * @param type the type of object
	 * @param value the value. For DICTIONARY, this is a HashMap. for ARRAY it's an ArrayList. For NUMBER, it's a Double. for BOOLEAN, it's Boolean.TRUE or
	 *            Boolean.FALSE. For everything else, it's a String.
	 */
	public PDFObject(PDFObjectOwner owner, int type, Object value) {
		this.type = type;
		if (type == NAME) {
			value = ((String) value).intern();
		} else if (type == KEYWORD && value.equals("true")) {
			this.type = BOOLEAN;
			value = Boolean.TRUE;
		} else if (type == KEYWORD && value.equals("false")) {
			this.type = BOOLEAN;
			value = Boolean.FALSE;
		}
		this.value = value;
		this.owner = owner;
	}

	public Object getValue() {
		return value;
	}

	/**
	 * create a new PDFObject that is the closest match to a given Java object. Possibilities include Double, String, PDFObject[], HashMap, Boolean, or
	 * PDFParser.Tok, which should be "true" or "false" to turn into a BOOLEAN.
	 *
	 * @param obj the sample Java object to convert to a PDFObject.
	 * @throws PDFParseException if the object isn't one of the above examples, and can't be turned into a PDFObject.
	 */
	public PDFObject(Object obj)
			throws PDFParseException {
		this.owner = null;
		this.value = obj;
		if ((obj instanceof Double) || (obj instanceof Integer)) {
			this.type = NUMBER;
		} else if (obj instanceof String) {
			this.type = NAME;
		} else if (obj instanceof PDFObject[]) {
			this.type = ARRAY;
		} else if (obj instanceof Object[]) {
			Object[] srcary = (Object[]) obj;
			PDFObject[] dstary = new PDFObject[srcary.length];
			for (int i = 0; i < srcary.length; i++) {
				dstary[i] = new PDFObject(srcary[i]);
			}
			value = dstary;
			this.type = ARRAY;
		} else if (obj instanceof HashMap) {
			this.type = DICTIONARY;
		} else if (obj instanceof Boolean) {
			this.type = BOOLEAN;
		} else {
			throw new PDFParseException("Bad type for raw PDFObject: " + obj);
		}
	}

	public PDFObject(HashMap<String, PDFObject> map) {
		this.type = DICTIONARY;
		this.value = map;
	}

	@SuppressWarnings("unused")
	public PDFObject(Object obj, boolean ignoreException) {
		try {
			this.owner = null;
			this.value = obj;
			if ((obj instanceof Double) || (obj instanceof Integer)) {
				this.type = NUMBER;
			} else if (obj instanceof String) {
				this.type = NAME;
			} else if (obj instanceof PDFObject[]) {
				this.type = ARRAY;
			} else if (obj instanceof Object[]) {
				Object[] srcary = (Object[]) obj;
				PDFObject[] dstary = new PDFObject[srcary.length];
				for (int i = 0; i < srcary.length; i++) {
					dstary[i] = new PDFObject(srcary[i]);
				}
				value = dstary;
				this.type = ARRAY;
			} else if (obj instanceof HashMap) {
				this.type = DICTIONARY;
			} else if (obj instanceof Boolean) {
				this.type = BOOLEAN;
			} else {
				throw new PDFParseException("Bad type for raw PDFObject: " + obj);
			}
		} catch (PDFParseException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * create a new PDFObject based on a PDFXref
	 *
	 * @param owner the PDFFile from which the PDFXref was drawn
	 * @param xref the PDFXref to turn into a PDFObject
	 */
	public PDFObject(PDFObjectOwner owner, PDFXref xref) {
		this.type = INDIRECT;
		this.value = xref;
		this.owner = owner;
	}

	/**
	 * get the type of this object. The object will be dereferenced, so INDIRECT will never be returned.
	 *
	 * @return the type of the object
	 */
	public int getType()
			throws PDFParseException {
		if (type == INDIRECT)
			return dereference().getType();
		return type;
	}

	/**
	 * set the stream of this object. It should have been a DICTIONARY before the call.
	 *
	 * @param data the data, as a ByteBuffer.
	 */
	public void setStream(PDF_BufferWrapper data) {
		this.type = STREAM;
		this.stream = data;
	}

	/**
	 * get the stream from this object. Will return null if this object isn't a STREAM.
	 *
	 * @return the stream, or null, if this isn't a STREAM.
	 */
	public byte[] getStream()
			throws PDFParseException {
		if (type == INDIRECT) {
			return dereference().getStream();
		} else if (type == STREAM && stream != null) {
			byte[] data = null;

			synchronized (stream) {
				// decode
				PDF_BufferWrapper streamBuf = decodeStream();
				// ByteBuffer streamBuf = stream;

				// First try to use the array with no copying. This can only
				// be done if the buffer has a backing array, and is not a slice
				if (streamBuf.hasArray() && streamBuf.arrayOffset() == 0) {
					byte[] ary = streamBuf.array();

					// make sure there is no extra data in the buffer
					if (ary.length == streamBuf.remaining()) {
						return ary;
					}
				}

				// Can't use the direct buffer, so copy the data (bad)
				data = new byte[streamBuf.remaining()];
				streamBuf.get(data);

				// return the stream to its starting position
				streamBuf.flip();
			}

			return data;
		} else if (type == STRING) {
			return PDFStringUtil.asBytes(getStringValue());
		}

		// wrong type
		return null;
	}

	/**
	 * get the stream from this object as a byte buffer. Will return null if this object isn't a STREAM.
	 *
	 * @return the buffer, or null, if this isn't a STREAM.
	 */
	public PDF_BufferWrapper getStreamBuffer()
			throws PDFParseException {
		if (type == INDIRECT) {
			return dereference().getStreamBuffer();
		} else if (type == STREAM && stream != null) {
			synchronized (stream) {
				PDF_BufferWrapper streamBuf = decodeStream();
				// ByteBuffer streamBuf = stream;
				return streamBuf.duplicate();
			}
		} else if (type == STRING) {
			String src = getStringValue();
			return owner.createBuffer(src.getBytes());
		}

		// wrong type
		return null;
	}

	/**
	 * Get the decoded stream value
	 *
	 * @throws PDFParseException
	 */
	private PDF_BufferWrapper decodeStream()
			throws PDFParseException {
		PDF_BufferWrapper outStream = null;

		// first try the cache
		if (decodedStream != null) {
			outStream = decodedStream;
		}

		// no luck in the cache, do the actual decoding
		if (outStream == null) {
			stream.rewind();
			outStream = owner.decodeStream(this, stream);
			decodedStream = outStream;
		}

		return outStream;
	}

	/**
	 * Get the value as a text string; i.e., a string encoded in UTF-16BE or PDFDocEncoding. Simple latin alpha-numeric characters are preserved in both these
	 * encodings.
	 *
	 * @return the text string value
	 * @throws PDFParseException
	 */
	public String getTextStringValue()
			throws PDFParseException {
		return PDFStringUtil.asTextString(getStringValue());
	}

	/**
	 * returns true only if this object is a DICTIONARY or a STREAM, and the "Type" entry in the dictionary matches a given value.
	 *
	 * @param match the expected value for the "Type" key in the dictionary
	 * @return whether the dictionary is of the expected type
	 */
	public boolean isDictType(String match)
			throws PDFParseException {
		if (type == INDIRECT) {
			return dereference().isDictType(match);
		} else if (type != DICTIONARY && type != STREAM) {
			return false;
		}

		PDFObject obj = getDictRef("Type");
		return obj != null && obj.getStringValue().equals(match);
	}

	public PDFDecrypter getDecrypter() {
		// PDFObjects without owners are always created as part of
		// content instructions. Such objects will never have encryption
		// applied to them, as the stream they're contained by is the
		// unit of encryption. So, if someone asks for the decrypter for
		// one of these in-stream objects, no decryption should
		// ever be applied. This can be seen with inline images.
		return owner != null ? owner.getDefaultDecrypter() : IdentityDecrypter.getInstance();
	}

	/**
	 * Set the object identifiers
	 *
	 * @param objNum the object number
	 * @param objGen the object generation number
	 */
	public void setObjectId(int objNum, int objGen) {
		assert objNum >= OBJ_NUM_TRAILER;
		assert objGen >= OBJ_NUM_TRAILER;
		this.objNum = objNum;
		this.objGen = objGen;
	}

	/**
	 * Get the object number of this object; a negative value indicates that the object is not numbered, as it's not a top-level object: if the value is
	 * {@link #OBJ_NUM_EMBEDDED}, it is because it's embedded within another object. If the value is {@link #OBJ_NUM_TRAILER}, it's because it's an object from
	 * the trailer.
	 *
	 * @return the object number, if positive
	 */
	public int getObjNum() {
		return objNum;
	}

	/**
	 * Get the object generation number of this object; a negative value indicates that the object is not numbered, as it's not a top-level object: if the value
	 * is {@link #OBJ_NUM_EMBEDDED}, it is because it's embedded within another object. If the value is {@link #OBJ_NUM_TRAILER}, it's because it's an object
	 * from the trailer.
	 *
	 * @return the object generation number, if positive
	 */
	public int getObjGen() {
		return objGen;
	}

	/**
	 * return a representation of this PDFObject as a String. Does NOT dereference anything: this is the only method that allows you to distinguish an INDIRECT
	 * PDFObject.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		try {
			if (type == INDIRECT) {
				StringBuffer str = new StringBuffer();
				str.append("Indirect to #" + ((PDFXref) value).getID() + (((PDFXref) value).getCompressed() ? " comp" : ""));
				try {
					// TODO [FHe] uncomment
					// str.append("\n" + dereference().toString());
					PDFObject obj = cachedDereference();
					str.append("\n" + (obj == null ? "<ref>" : obj.toString()));
				} catch (Throwable t) {
					str.append(t.toString());
				}
				return str.toString();
			} else if (type == BOOLEAN) {
				return "Boolean: " + (getBooleanValue() ? "true" : "false");
			} else if (type == NUMBER) {
				return "Number: " + getDoubleValue();
			} else if (type == STRING) {
				return "String: " + getStringValue();
			} else if (type == NAME) {
				return "Name: /" + getStringValue();
			} else if (type == ARRAY) {
				return "Array, length=" + ((PDFObject[]) value).length;
			} else if (type == DICTIONARY) {
				StringBuffer sb = new StringBuffer();
				PDFObject obj = getDictRef("Type");
				if (obj != null) {
					sb.append(obj.getStringValue());
					obj = getDictRef("Subtype");
					if (obj == null) {
						obj = getDictRef("S");
					}
					if (obj != null) {
						sb.append("/" + obj.getStringValue());
					}
				} else {
					sb.append("Untyped");
				}
				sb.append(" dictionary. Keys:");
				HashMap<Object, Object> hm = (HashMap<Object, Object>) value;
				Iterator<Entry<Object, Object>> it = hm.entrySet().iterator();
				Entry<Object, Object> entry;
				while (it.hasNext()) {
					entry = it.next();
					sb.append("\n   " + entry.getKey() + "  " + entry.getValue());
				}
				return sb.toString();
			} else if (type == STREAM) {
				byte[] st = getStream();
				if (st == null) {
					return "Broken stream";
				}
				return "Stream: [[" + new String(st, 0, st.length > 30 ? 30 : st.length) + "]]";
			} else if (type == NULL) {
				return "Null";
			} else if (type == KEYWORD) {
				return "Keyword: " + getStringValue();
				/*
				 * } else if (type==IMAGE) { StringBuffer sb= new StringBuffer(); java.awt.Image im= (java.awt.Image)stream;
				 * sb.append("Image ("+im.getWidth(null)+"x"+im.getHeight(null)+", with keys:"); HashMap hm= (HashMap)value; Iterator it=
				 * hm.keySet().iterator(); while(it.hasNext()) { sb.append(" "+(String)it.next()); } return sb.toString();
				 */
			} else {
				return "Whoops!  big error!  Unknown type";
			}
		} catch (IOException ioe) {
			return "Caught an error: " + ioe;
		}
	}

	/**
	 * Make sure that this object is dereferenced. Use the cache of an indirect object to cache the dereferenced value, if possible.
	 */
	public PDFObject dereference()
			throws PDFParseException {
		if (type != INDIRECT)
			return this;

		if (cache == null || ((PDFObject) cache).value == null) {
			if (owner == null) {
				System.out.println("Bad seed (owner==null)!  Object=" + this);
			}

			cache = owner.dereferenceOld((PDFXref) value, getDecrypter());
		}

		return ((PDFObject) cache);
	}

	/**
	 * Make sure that this object is dereferenced. Use the cache of an indirect object to cache the dereferenced value, if possible.
	 */
	public PDFObject cachedDereference()
			throws PDFParseException {
		if (type != INDIRECT)
			return this;

		if (cache == null || ((PDFObject) cache).value == null) {
			if (owner == null) {
				System.out.println("Bad seed (owner==null)!  Object=" + this);
			}

			cache = owner.dereferenceOld((PDFXref) value, getDecrypter());
		}

		return ((PDFObject) cache);
	}

	/**
	 * Identify whether the object is currently an indirect/cross-reference
	 *
	 * @return whether currently indirect
	 */
	public boolean isIndirect() {
		return (type == INDIRECT);
	}

	/**
	 * Test whether two PDFObject are equal. Objects are equal IFF they are the same reference OR they are both indirect objects with the same id and generation
	 * number in their xref
	 */
	@Override
	public boolean equals(Object o) {
		if (super.equals(o)) {
			// they are the same object
			return true;
		} else if (type == INDIRECT && o instanceof PDFObject) {
			// they are both PDFObjects. Check type and xref.
			PDFObject obj = (PDFObject) o;

			if (obj.type == INDIRECT) {
				PDFXref lXref = (PDFXref) value;
				PDFXref rXref = (PDFXref) obj.value;

				return ((lXref.getID() == rXref.getID()) && (lXref.getGeneration() == rXref.getGeneration()) && (lXref.getCompressed() == rXref.getCompressed()));
			}
		}

		return false;
	}

	/**
	 * @return The current cache item stored. (MAY become null with any GC call)
	 */
	public Object getCache()
			throws PDFParseException {
		if (type == INDIRECT)
			return dereference().getCache();

		return cache;
	}

	/**
	 * @param cacheItem The current cache item to stored. (MAY become null with any GC call)
	 */
	public void setCache(Object cacheItem)
			throws PDFParseException {
		if (type == INDIRECT) {
			dereference().setCache(cacheItem);
			return;
		}

		cache = cacheItem;
	}

	/**
	 * @return The <b>int</b> value of this object.
	 *
	 * @throws PDFParseException If this object does not contain a number.
	 */
	public int getIntValue()
			throws PDFParseException {
		if (type == INDIRECT)
			return dereference().getIntValue();

		if (type == NUMBER)
			return ((Double) value).intValue();

		throw new PDFParseException("Not a Number: " + this);
	}

	/**
	 * @return The <b>float</b> value of this object.
	 *
	 * @throws PDFParseException If this object does not contain a number.
	 */
	public float getFloatValue()
			throws PDFParseException {
		if (type == INDIRECT)
			return dereference().getFloatValue();

		if (type == NUMBER)
			return ((Double) value).floatValue();

		throw new PDFParseException("Not a Number: " + this);
	}

	/**
	 * @return The <b>double</b> value of this object.
	 *
	 * @throws PDFParseException If this object does not contain a number.
	 */
	public double getDoubleValue()
			throws PDFParseException {
		if (type == INDIRECT)
			return dereference().getDoubleValue();

		if (type == NUMBER)
			return ((Double) value).doubleValue();

		throw new PDFParseException("Not a Number: " + this);
	}

	/**
	 * This method will <b>NOT</b> convert a NUMBER to a String.<br>
	 * If the string is actually a text string (i.e., may be encoded in UTF16-BE or PdfDocEncoding), then one should use {@link #getTextStringValue()} or use
	 * one of the {@link PDFStringUtil} methods on the result from this method. <br>
	 * The string value represents exactly the sequence of 8 bit characters present in the file, decrypted and decoded as appropriate, into a string containing
	 * only 8 bit character values - that is, each char will be between 0 and 255.
	 *
	 * @return A <b>String</b> value of this object, if its of type STRING, NAME, or KEYWORD.
	 *
	 * @throws PDFParseException If this object does not contain a number.
	 */
	public String getStringValue()
			throws PDFParseException {
		if (type == INDIRECT)
			return dereference().getStringValue();

		if (type == STRING || type == NAME || type == KEYWORD)
			return (String) value;

		throw new PDFParseException("Not a String: " + this);
	}

	/**
	 * @param index The index of the desired element in the array.
	 * @return The object which at the specified index.
	 *
	 * @throws PDFParseException If this object is not an Array.
	 */
	public PDFObject getAt(int index)
			throws PDFParseException {
		return getArray()[index];
	}

	/**
	 * @return The <b>Array</b> value of this object, or <b>this object as an array with length == 1</b>.
	 *
	 * @throws PDFParseException If this object is not an Array.
	 */
	public PDFObject[] getArray()
			throws PDFParseException {
		if (type == INDIRECT) {
			PDFObject dereference = dereference();
			return dereference.getArray();
		}
		if (type == ARRAY)
			return (PDFObject[]) value;

		PDFObject[] ary = new PDFObject[1];
		ary[0] = this;
		return ary;
	}

	/**
	 * @return The <b>boolean</b> value of this object.
	 *
	 * @throws PDFParseException If this object does not contain a number.
	 */
	public boolean getBooleanValue()
			throws PDFParseException {
		if (type == INDIRECT)
			return dereference().getBooleanValue();

		if (type == BOOLEAN)
			return value == Boolean.TRUE;

		throw new PDFParseException("Not a Boolean: " + this);
	}

	/**
	 * get the dictionary as a HashMap. If this isn't a DICTIONARY or a STREAM, returns null
	 */
	@SuppressWarnings("unchecked")
	public HashMap<String, PDFObject> getDictionary()
			throws PDFParseException {
		if (type == INDIRECT) {
			PDFObject obj = dereference();
			return obj.getDictionary();
		}

		if (type == DICTIONARY || type == STREAM)
			return (HashMap<String, PDFObject>) value;

		throw new PDFParseException("Not A Dictionary: " + this);
	}

	/**
	 * @return An iterator with the keys of the dictionary.
	 *
	 * @throws PDFParseException If this object is not a dictionary.
	 */
	public Iterator<String> getDictKeys()
			throws PDFParseException {
		return getDictionary().keySet().iterator();
	}

	/**
	 * @return An iterator with the keys of the dictionary.
	 *
	 * @throws PDFParseException If this object is not a dictionary.
	 */
	public String[] getDictKeysAsArray()
			throws PDFParseException {
		Set<String> keySet = getDictionary().keySet();
		return keySet.toArray(new String[keySet.size()]);
	}

	/**
	 * @param key The key of the item in the dictionary.
	 * @return A {@link PDFObject} matching the specified key in the dictionary.
	 *
	 * @throws PDFParseException If this object is not a dictionary.
	 */
	public PDFObject getDictRef(String key)
			throws PDFParseException {
		HashMap<String, PDFObject> map = getDictionary();
		return map.get(key.intern());
	}

	public PDFObject findNestedResource(String dictName, String refName)
			throws PDFParseException {
		PDFObject in = getDictRef(dictName);
		return in.getDictRef(refName);
	}

	public PDFObject deepTreeSearch(String key)
			throws PDFParseException {
		// first, look for a Names entry, meaning this is a leaf
		PDFObject names = getDictRef("Names");
		if (names != null)
			return findInArray(names.getArray(), key);

		// no names given, look for kids
		PDFObject kidsObj = getDictRef("Kids");
		if (kidsObj == null)
			return null;

		PDFObject[] kids = kidsObj.getArray();
		for (int i = 0; i < kids.length; i++) {
			PDFObject limitsObj = kids[i].getDictRef("Limits");
			if (limitsObj == null)
				continue;

			String lowerLimit = limitsObj.getAt(0).getStringValue();
			String upperLimit = limitsObj.getAt(1).getStringValue();
			if ((key.compareTo(lowerLimit) < 0) || (key.compareTo(upperLimit) > 0))
				continue;

			return kids[i].deepTreeSearch(key);
		}

		return null;
	}

	/**
	 * Find an object in a (key,value) array. Do this by splitting in half repeatedly.
	 */
	private PDFObject findInArray(PDFObject[] array, String key)
			throws PDFParseException {
		int start = 0;
		int end = array.length / 2;

		while (end >= start && start >= 0 && end < array.length) {
			// find the key at the midpoint
			int pos = start + ((end - start) / 2);
			String posKey = array[pos * 2].getStringValue();

			// compare the key to the key we are looking for
			int comp = key.compareTo(posKey);
			if (comp == 0) {
				// they match. Return the value
				return array[(pos * 2) + 1];
			} else if (comp > 0) {
				// too big, search the top half of the tree
				start = pos + 1;
			} else if (comp < 0) {
				// too small, search the bottom half of the tree
				end = pos - 1;
			}
		}

		// not found
		return null;
	}

	public PDFObjectOwner getFile() {
		return owner;
	}

	public int[] getAsIntArray()
			throws PDFParseException {
		PDFObject[] values = getArray();
		int ints[] = new int[values.length];
		for (int i = 0; i < ints.length; i++) {
			ints[i] = values[i].getIntValue();
		}
		return ints;
	}

	public float[] getAsFloatArray()
			throws PDFParseException {
		PDFObject[] values = getArray();
		float floats[] = new float[values.length];
		for (int i = 0; i < floats.length; i++) {
			floats[i] = values[i].getFloatValue();
		}
		return floats;
	}

	public boolean isBoolean()
			throws PDFParseException {
		return getType() == PDFObject.BOOLEAN;
	}

	public boolean isNumber()
			throws PDFParseException {
		return getType() == PDFObject.NUMBER;
	}

	public boolean isString()
			throws PDFParseException {
		return getType() == PDFObject.STRING;
	}

	public boolean isName()
			throws PDFParseException {
		return getType() == PDFObject.NAME;
	}

	public boolean isArray()
			throws PDFParseException {
		return getType() == PDFObject.ARRAY;
	}

	public boolean isDictionary()
			throws PDFParseException {
		return getType() == PDFObject.DICTIONARY;
	}

	public boolean isStream()
			throws PDFParseException {
		return getType() == PDFObject.STREAM;
	}

	public boolean isKeyword()
			throws PDFParseException {
		return getType() == PDFObject.KEYWORD;
	}

	public PDF_BufferWrapper getRawStream() {
		return stream;
	}

}
