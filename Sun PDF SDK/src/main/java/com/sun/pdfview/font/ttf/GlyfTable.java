/*
 * $Id: GlyfTable.java,v 1.2 2007/12/20 18:33:30 rbair Exp $
 *
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

package com.sun.pdfview.font.ttf;


import com.nu.art.software.pdf.core.PDF_BufferWrapper;
import com.nu.art.software.pdf.sdk.android.core.PDF_NioBuffer;


/**
 * Model the TrueType Glyf table
 */
public class GlyfTable
		extends TrueTypeTable {

	/**
	 * the glyph data, as either a byte buffer (unparsed) or a glyph object (parsed)
	 */
	private Object[] glyphs;

	/**
	 * The glyph location table
	 */
	private LocaTable loca;

	/** Creates a new instance of HmtxTable */
	protected GlyfTable(TrueTypeFont ttf) {
		super(TrueTypeTable.GLYF_TABLE);

		loca = (LocaTable) ttf.getTable("loca");

		MaxpTable maxp = (MaxpTable) ttf.getTable("maxp");
		int numGlyphs = maxp.getNumGlyphs();

		glyphs = new Object[numGlyphs];
	}

	/**
	 * Get the glyph at a given index, parsing it as needed
	 */
	public Glyf getGlyph(int index) {
		Object o;
		try {
			o = glyphs[index];
		} catch (Exception e) {
			return null;
		}
		if (o == null) {
			return null;
		}

		if (o instanceof PDF_BufferWrapper) {
			Glyf g = Glyf.getGlyf((PDF_BufferWrapper) o);
			glyphs[index] = g;

			return g;
		}
		return (Glyf) o;
	}

	/** get the data in this map as a ByteBuffer */
	@Override
	public PDF_BufferWrapper getData() {
		int size = getLength();

		PDF_BufferWrapper buf = new PDF_NioBuffer(size);

		// write the offsets
		for (int i = 0; i < glyphs.length; i++) {
			Object o = glyphs[i];
			if (o == null) {
				continue;
			}

			PDF_BufferWrapper glyfData = null;
			if (o instanceof PDF_BufferWrapper) {
				glyfData = (PDF_BufferWrapper) o;
			} else {
				glyfData = ((Glyf) o).getData();
			}

			glyfData.rewind();
			buf.put(glyfData);
			glyfData.flip();
		}

		// reset the start pointer
		buf.flip();

		return buf;
	}

	/** Initialize this structure from a ByteBuffer */
	@Override
	public void setData(PDF_BufferWrapper data) {
		for (int i = 0; i < glyphs.length; i++) {
			int location = loca.getOffset(i);
			int length = loca.getSize(i);

			if (length == 0) {
				// undefined glyph
				continue;
			}

			data.position(location);
			PDF_BufferWrapper glyfData = data.slice();
			glyfData.limit(length);

			glyphs[i] = glyfData;
		}
	}

	/**
	 * Get the length of this table
	 */
	@Override
	public int getLength() {
		int length = 0;

		for (int i = 0; i < glyphs.length; i++) {
			Object o = glyphs[i];
			if (o == null) {
				continue;
			}

			if (o instanceof PDF_BufferWrapper) {
				length += ((PDF_BufferWrapper) o).remaining();
			} else {
				length += ((Glyf) o).getLength();
			}
		}

		return length;
	}

	/**
	 * Create a pretty String
	 */
	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		String indent = "    ";

		buf.append(indent + "Glyf Table: (" + glyphs.length + " glyphs)\n");
		buf.append(indent + "  Glyf 0: " + getGlyph(0));

		return buf.toString();
	}
}
