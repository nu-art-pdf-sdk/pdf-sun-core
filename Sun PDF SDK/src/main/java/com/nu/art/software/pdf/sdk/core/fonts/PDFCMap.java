/*
 * $Id: PDFCMap.java,v 1.3 2009/02/12 13:53:54 tomoke Exp $
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
package com.nu.art.software.pdf.sdk.core.fonts;


import java.util.HashMap;

import com.sun.pdfview.core.PDFObject;
import com.sun.pdfview.core.PDFParseException;


public abstract class PDFCMap {

	private static HashMap<String, PDFCMap> cache;

	protected PDFCMap() {}

	public static PDFCMap getCMap(PDFObject map)
			throws PDFParseException {
		if (map.isName()) {
			return getCMap(map.getStringValue());
		} else if (map.isStream()) {
			throw new PDFParseException("Parsing CMap Files Unsupported!");
		} else {
			throw new PDFParseException("CMap type not Name or Stream!");
		}
	}

	public static PDFCMap getCMap(String mapName)
			throws PDFParseException {
		if (cache == null) {
			populateCache();
		}

		if (!cache.containsKey(mapName)) {
			throw new PDFParseException("Unknown CMap: " + mapName);
		}

		return cache.get(mapName);
	}

	protected static void populateCache() {
		cache = new HashMap<String, PDFCMap>();
		cache.put("Identity-H", new PDFCMap() {

			@Override
			public char map(char src) {
				return src;
			}
		});
	}

	public abstract char map(char src);

	public int getFontID(char src) {
		return 0;
	}

}
