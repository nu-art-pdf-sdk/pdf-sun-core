/*
 * Copyright (c) 2014 to Adam van der Kruk (Zehavi) AKA TacB0sS - Nu-Art Software
 * Licensed under the Binpress license;
 */
package com.nu.art.software.pdf.sdk.android.core;


import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;

import com.nu.art.software.pdf.core.PDF_BufferWrapper;


public class PDF_NioBuffer
		implements PDF_BufferWrapper {

	private ByteBuffer actualBuffer;

	public PDF_NioBuffer(int capacity) {
		actualBuffer = java.nio.ByteBuffer.allocate(capacity);
	}

	public PDF_NioBuffer(byte[] bytes) {
		actualBuffer = java.nio.ByteBuffer.wrap(bytes);
	}

	public PDF_NioBuffer(ByteBuffer buffer) {
		actualBuffer = buffer;
	}

	@Override
	public byte[] array() {
		return actualBuffer.array();
	}

	@Override
	public int arrayOffset() {
		return actualBuffer.arrayOffset();
	}

	public CharBuffer asCharBuffer() {
		return actualBuffer.asCharBuffer();
	}

	public DoubleBuffer asDoubleBuffer() {
		return actualBuffer.asDoubleBuffer();
	}

	public FloatBuffer asFloatBuffer() {
		return actualBuffer.asFloatBuffer();
	}

	public IntBuffer asIntBuffer() {
		return actualBuffer.asIntBuffer();
	}

	public LongBuffer asLongBuffer() {
		return actualBuffer.asLongBuffer();
	}

	public java.nio.ByteBuffer asReadOnlyBuffer() {
		return actualBuffer.asReadOnlyBuffer();
	}

	public ShortBuffer asShortBuffer() {
		return actualBuffer.asShortBuffer();
	}

	public int capacity() {
		return actualBuffer.capacity();
	}

	public Buffer clear() {
		return actualBuffer.clear();
	}

	public java.nio.ByteBuffer compact() {
		return actualBuffer.compact();
	}

	public int compareTo(final java.nio.ByteBuffer otherBuffer) {
		return actualBuffer.compareTo(otherBuffer);
	}

	@Override
	public PDF_NioBuffer duplicate() {
		return new PDF_NioBuffer(actualBuffer.duplicate());
	}

	@Override
	public boolean equals(final Object other) {
		return actualBuffer.equals(other);
	}

	@Override
	public void flip() {
		actualBuffer.flip();
	}

	@Override
	public byte get() {
		return actualBuffer.get();
	}

	@Override
	public void get(final byte[] dest, final int off, final int len) {
		actualBuffer.get(dest, off, len);
	}

	@Override
	public void get(final byte[] dest) {
		actualBuffer.get(dest);
	}

	@Override
	public byte get(final int index) {
		return actualBuffer.get(index);
	}

	@Override
	public char getChar() {
		return actualBuffer.getChar();
	}

	@Override
	public char getChar(final int index) {
		return actualBuffer.getChar(index);
	}

	public double getDouble() {
		return actualBuffer.getDouble();
	}

	public double getDouble(final int index) {
		return actualBuffer.getDouble(index);
	}

	public float getFloat() {
		return actualBuffer.getFloat();
	}

	public float getFloat(final int index) {
		return actualBuffer.getFloat(index);
	}

	@Override
	public int getInt() {
		return actualBuffer.getInt();
	}

	public int getInt(final int index) {
		return actualBuffer.getInt(index);
	}

	@Override
	public long getLong() {
		return actualBuffer.getLong();
	}

	public long getLong(final int index) {
		return actualBuffer.getLong(index);
	}

	@Override
	public short getShort() {
		return actualBuffer.getShort();
	}

	public short getShort(final int index) {
		return actualBuffer.getShort(index);
	}

	@Override
	public boolean hasArray() {
		return actualBuffer.hasArray();
	}

	@Override
	public int hashCode() {
		return actualBuffer.hashCode();
	}

	@Override
	public boolean hasRemaining() {
		return actualBuffer.hasRemaining();
	}

	public boolean isDirect() {
		return actualBuffer.isDirect();
	}

	public boolean isReadOnly() {
		return actualBuffer.isReadOnly();
	}

	@Override
	public int limit() {
		return actualBuffer.limit();
	}

	@Override
	public void limit(final int newLimit) {
		actualBuffer.limit(newLimit);
	}

	@Override
	public void mark() {
		actualBuffer.mark();
	}

	public ByteOrder order() {
		return actualBuffer.order();
	}

	public java.nio.ByteBuffer order(final ByteOrder byteOrder) {
		return actualBuffer.order(byteOrder);
	}

	@Override
	public int position() {
		return actualBuffer.position();
	}

	@Override
	public void position(final int newPosition) {
		actualBuffer.position(newPosition);
	}

	@Override
	public void put(final byte b) {
		actualBuffer.put(b);
	}

	@Override
	public void put(final byte[] src) {
		actualBuffer.put(src);
	}

	public PDF_BufferWrapper put(final java.nio.ByteBuffer src) {
		actualBuffer.put(src);
		return this;
	}

	@Override
	public void put(final PDF_BufferWrapper src) {
		actualBuffer.put(src.getBuffer());
	}

	@Override
	public void put(final int index, final byte b) {
		actualBuffer.put(index, b);
	}

	@Override
	public void putChar(final char value) {
		actualBuffer.putChar(value);
	}

	public PDF_BufferWrapper put(final byte[] src, final int off, final int len) {
		actualBuffer.put(src, off, len);
		return this;
	}

	public PDF_BufferWrapper putChar(final int index, final char value) {
		actualBuffer.putChar(index, value);
		return this;
	}

	public PDF_BufferWrapper putDouble(final double value) {
		actualBuffer.putDouble(value);
		return this;
	}

	public PDF_BufferWrapper putDouble(final int index, final double value) {
		actualBuffer.putDouble(index, value);
		return this;
	}

	public PDF_BufferWrapper putFloat(final float value) {
		actualBuffer.putFloat(value);
		return this;
	}

	public PDF_BufferWrapper putFloat(final int index, final float value) {
		actualBuffer.putFloat(index, value);
		return this;
	}

	@Override
	public void putInt(final int index, final int value) {
		actualBuffer.putInt(index, value);
	}

	@Override
	public void putInt(final int value) {
		actualBuffer.putInt(value);
	}

	public void putLong(final int index, final long value) {
		actualBuffer.putLong(index, value);
	}

	@Override
	public void putLong(final long value) {
		actualBuffer.putLong(value);
	}

	public void putShort(final int index, final short value) {
		actualBuffer.putShort(index, value);
	}

	@Override
	public void putShort(final short value) {
		actualBuffer.putShort(value);
	}

	@Override
	public int remaining() {
		return actualBuffer.remaining();
	}

	@Override
	public void reset() {
		actualBuffer.reset();
	}

	@Override
	public void rewind() {
		actualBuffer.rewind();
	}

	@Override
	public ByteBuffer getBuffer() {
		return actualBuffer;
	}

	@Override
	public PDF_BufferWrapper slice() {
		return new PDF_NioBuffer(actualBuffer.slice());
	}

	@Override
	public PDF_BufferWrapper sliceFromTo(int start, int ending) {
		int pos = position();
		position(start);
		PDF_BufferWrapper sliced = slice();
		sliced.limit(ending - start);
		position(pos);
		return sliced;
	}
}
