package com.nu.art.software.pdf.sdk.core.fonts;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class PDFFont {

	private PDFFontEncoding encoding;

	private PDFFontDescriptor descriptor;

	private Map<Character, PDFGlyph> charCache = new HashMap<Character, PDFGlyph>();

	protected PDFFont(PDFFontDescriptor descriptor) {
		this();
		setDescriptor(descriptor);
	}

	public PDFFont() {}

	public PDFFontEncoding getEncoding() {
		return encoding;
	}

	public void setEncoding(PDFFontEncoding encoding) {
		this.encoding = encoding;
	}

	public PDFFontDescriptor getDescriptor() {
		return descriptor;
	}

	public void setDescriptor(PDFFontDescriptor descriptor) {
		this.descriptor = descriptor;
	}

	public List<PDFGlyph> getGlyphs(String text) {
		List<PDFGlyph> outList = null;

		// if we have an encoding, use it to get the commands
		if (encoding != null) {
			outList = encoding.getGlyphs(this, text);
		} else {
			// use the default mapping
			char[] arry = text.toCharArray();
			outList = new ArrayList<PDFGlyph>(arry.length);

			for (int i = 0; i < arry.length; i++) {
				// only look at 2 bytes when there is no encoding
				char src = (char) (arry[i] & 0xff);
				outList.add(getCachedGlyph(src, null));
			}
		}

		return outList;
	}

	/**
	 * Get a glyph for a given character code. The glyph is returned from the cache if available, or added to the cache if not
	 *
	 * @param src the character code of this glyph
	 * @param name the name of the glyph, or null if the name is unknown
	 * @return a glyph for this character
	 */
	public final PDFGlyph getCachedGlyph(char src, String name) {

		PDFGlyph glyph = charCache.get(src);

		if (glyph == null) {
			glyph = getGlyph(src, name);
			charCache.put(Character.valueOf(src), glyph);
		}

		return glyph;
	}

	/**
	 * Get the glyph for a given character code and name
	 *
	 * The preferred method of getting the glyph should be by name. If the name is null or not valid, then the character code should be used. If the both the
	 * code and the name are invalid, the undefined glyph should be returned.
	 *
	 * Note this method must *always* return a glyph.
	 *
	 * @param src the character code of this glyph
	 * @param name the name of this glyph or null if unknown
	 * @return a glyph for this character
	 */
	public abstract PDFGlyph getGlyph(char src, String name);
}
