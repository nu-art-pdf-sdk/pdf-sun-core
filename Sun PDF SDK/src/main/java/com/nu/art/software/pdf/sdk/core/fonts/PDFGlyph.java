/*
 * $Id: PDFGlyph.java,v 1.3 2009/02/09 16:35:01 tomoke Exp $
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

package com.nu.art.software.pdf.sdk.core.fonts;


import android.graphics.Path;
import android.graphics.PointF;


public class PDFGlyph {

	private char src;

	private String name;

	private PointF advance;

	private Path shape;

	// private _RenderingCommand[] commands;

	public PDFGlyph(char src, String name, Path shape, PointF advance) {
		this.shape = shape;
		this.setAdvance(advance);
		this.src = src;
		this.name = name;
	}

	// public PDFGlyph(char src, String name, PointF advance, _RenderingCommand... commands) {
	// this.commands = commands;
	// this.setAdvance(advance);
	// this.src = src;
	// this.name = name;
	// }

	public char getChar() {
		return src;
	}

	public String getName() {
		return name;
	}

	public Path getShape() {
		return shape;
	}

	// public _RenderingCommand[] getRenderingCommands() {
	// return commands;
	// }

	@Override
	public String toString() {
		StringBuffer str = new StringBuffer();
		if (name != null) {
			str.append(name);
			str.append(" = ");
		}

		str.append(src);
		return str.toString();
	}

	public PointF getAdvance() {
		return advance;
	}

	public void setAdvance(PointF advance) {
		this.advance = advance;
	}

	public void setRealChar(char mappedChar) {
		src = mappedChar;
	}
}
