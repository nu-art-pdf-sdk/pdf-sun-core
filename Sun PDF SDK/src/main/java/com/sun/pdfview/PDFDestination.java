/*
 * $Id: PDFDestination.java,v 1.3 2009/01/16 16:26:09 tomoke Exp $
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */
package com.sun.pdfview;


import com.sun.pdfview.core.PDFObject;


/**
 * Represents a destination in a PDF file. Destinations take 3 forms:
 * <ul>
 * <li>An explicit destination, which contains a reference to a page as well as some stuff about how to fit it into the window.
 * <li>A named destination, which uses the PDF file's Dests entry in the document catalog to map a name to an explicit destination
 * <li>A string destintation, which uses the PDF file's Dests entry. in the name directory to map a string to an explicit destination.
 * </ul>
 *
 * All three of these cases are handled by the getDestination() method.
 */
public class PDFDestination {

	/** the page we refer to */
	private PDFObject pageObj;

	/** the left coordinate of the fit area, if applicable */
	private float left;

	/** the right coordinate of the fit area, if applicable */
	private float right;

	/** the top coordinate of the fit area, if applicable */
	private float top;

	/** the bottom coordinate of the fit area, if applicable */
	private float bottom;

	/** the zoom, if applicable */
	private float zoom;

	/**
	 * Creates a new instance of PDFDestination
	 *
	 * @param pageObj the page object this destination refers to
	 * @param type the type of page this object refers to
	 */
	public PDFDestination(PDFObject pageObj) {
		this.pageObj = pageObj;
	}

	/**
	 * Get the PDF Page object associated with this destination
	 */
	public PDFObject getPage() {
		return pageObj;
	}

	/**
	 * Get the left coordinate value
	 */
	public float getLeft() {
		return left;
	}

	/**
	 * Set the left coordinate value
	 */
	public void setLeft(float left) {
		this.left = left;
	}

	/**
	 * Get the right coordinate value
	 */
	public float getRight() {
		return right;
	}

	/**
	 * Set the right coordinate value
	 */
	public void setRight(float right) {
		this.right = right;
	}

	/**
	 * Get the top coordinate value
	 */
	public float getTop() {
		return top;
	}

	/**
	 * Set the top coordinate value
	 */
	public void setTop(float top) {
		this.top = top;
	}

	/**
	 * Get the bottom coordinate value
	 */
	public float getBottom() {
		return bottom;
	}

	/**
	 * Set the bottom coordinate value
	 */
	public void setBottom(float bottom) {
		this.bottom = bottom;
	}

	/**
	 * Get the zoom value
	 */
	public float getZoom() {
		return zoom;
	}

	/**
	 * Set the zoom value
	 */
	public void setZoom(float zoom) {
		this.zoom = zoom;
	}

}
