/*
 * Copyright since 2014 belongs to: 'Adam van der Kruk (Zehavi)' AKA 'TacB0sS' and/or 'Nu-Art Software' Israel
 * ------
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

package com.nu.art.software.pdf.core;


import java.nio.ByteBuffer;


public interface PDF_BufferWrapper {

	int position();

	void position(int position);

	byte get();

	int remaining();

	byte get(int position);

	PDF_BufferWrapper slice();

	void limit(int length);

	void get(byte[] outBuf);

	void rewind();

	int limit();

	boolean hasArray();

	int arrayOffset();

	byte[] array();

	void flip();

	PDF_BufferWrapper duplicate();

	char getChar(int position);

	int getInt();

	long getLong();

	char getChar();

	short getShort();

	void put(int index, byte b);

	void put(byte b);

	void putInt(int i);

	void putShort(short s);

	void mark();

	void put(PDF_BufferWrapper data);

	void reset();

	void putInt(int index, int value);

	void putLong(long value);

	void putChar(char value);

	void put(byte[] data);

	void get(byte[] outBuf, int outOffset, int length);

	boolean hasRemaining();

	ByteBuffer getBuffer();

	PDF_BufferWrapper sliceFromTo(int start, int ending);

}
