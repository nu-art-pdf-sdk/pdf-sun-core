package com.sun.pdfview.decoders;


import com.nu.art.software.pdf.core.Decoder;
import com.nu.art.software.pdf.core.PDF_BufferWrapper;
import com.nu.art.software.pdf.sdk.android.core.PDF_NioBuffer;
import com.sun.pdfview.core.PDFObject;
import com.sun.pdfview.core.PDFParseException;


public final class CCITTFax_Decoder
		implements Decoder {

	@Override
	public PDF_BufferWrapper decode(PDF_BufferWrapper buffer, PDFObject params)
			throws PDFParseException {
		// ?3? in this specific implementation there might be a bug, I've switched the dict and params in the caller
		// since params was not used, and removed the dictionary to support Decoder interface!
		byte[] buf = new byte[buffer.remaining()];
		buffer.get(buf, 0, buf.length);

		int width = 1728;
		PDFObject widthDef = params.getDictRef("Width");
		if (widthDef == null) {
			widthDef = params.getDictRef("W");
		}
		if (widthDef != null) {
			width = widthDef.getIntValue();
		}
		int height = 0;
		PDFObject heightDef = params.getDictRef("Height");
		if (heightDef == null) {
			heightDef = params.getDictRef("H");
		}
		if (heightDef != null) {
			height = heightDef.getIntValue();
		}

		//
		int columns = getOptionFieldInt(params, "Columns", width);
		int rows = getOptionFieldInt(params, "Rows", height);
		int k = getOptionFieldInt(params, "K", 0);
		int size = rows * ((columns + 7) >> 3);
		byte[] destination = new byte[size];

		boolean align = getOptionFieldBoolean(params, "EncodedByteAlign", false);

		CCITTFax_DecoderHelper decoder = new CCITTFax_DecoderHelper(1, columns, rows);
		decoder.setAlign(align);
		if (k == 0) {
			decoder.decodeT41D(destination, buf, 0, rows);
		} else if (k > 0) {
			decoder.decodeT42D(destination, buf, 0, rows);
		} else if (k < 0) {
			decoder.decodeT6(destination, buf, 0, rows);
		}
		if (!getOptionFieldBoolean(params, "BlackIs1", false)) {
			for (int i = 0; i < destination.length; i++) {
				// bitwise not
				destination[i] = (byte) ~destination[i];
			}
		}

		return new PDF_NioBuffer(destination);
	}

	public static int getOptionFieldInt(PDFObject dict, String name, int defaultValue)
			throws PDFParseException {

		PDFObject dictParams = dict.getDictRef("DecodeParms");

		if (dictParams == null) {
			return defaultValue;
		}
		PDFObject value = dictParams.getDictRef(name);
		if (value == null) {
			return defaultValue;
		}
		return value.getIntValue();
	}

	public static boolean getOptionFieldBoolean(PDFObject dict, String name, boolean defaultValue)
			throws PDFParseException {

		PDFObject dictParams = dict.getDictRef("DecodeParms");

		if (dictParams == null) {
			return defaultValue;
		}
		PDFObject value = dictParams.getDictRef(name);
		if (value == null) {
			return defaultValue;
		}
		return value.getBooleanValue();
	}

}
