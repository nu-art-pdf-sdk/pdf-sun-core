/*
 * $Id: ASCIIHexDecode.java,v 1.2 2007/12/20 18:33:32 rbair Exp $
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 */

package com.sun.pdfview.decoders;


import java.io.ByteArrayOutputStream;

import com.nu.art.software.pdf.core.Decoder;
import com.nu.art.software.pdf.core.PDF_BufferWrapper;
import com.nu.art.software.pdf.sdk.android.core.PDF_NioBuffer;
import com.sun.pdfview.core.PDFObject;


/**
 * decode an array of hex nybbles into a byte array
 *
 * @author Mike Wessler
 */
public class ASCIIHex_Decoder
		extends _DecoderTools
		implements Decoder {

	/**
	 * decode an array of bytes in ASCIIHex format.
	 * <p>
	 * ASCIIHex format consists of a sequence of Hexidecimal digits, with possible whitespace, ending with the '&gt;' character.
	 *
	 * @param buf the encoded ASCII85 characters in a byte buffer
	 * @param params parameters to the decoder (ignored)
	 * @return the decoded bytes
	 */
	@Override
	public PDF_BufferWrapper decode(PDF_BufferWrapper buf, PDFObject params) {
		// start at the beginning of the buffer
		buf.rewind();

		// allocate the output buffer
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		while (true) {
			int first = readHexDigit(buf);
			int second = readHexDigit(buf);

			if (first == -1) {
				break;
			} else if (second == -1) {
				break;
			} else {
				baos.write((byte) ((first << 4) + second));
			}
		}

		return new PDF_NioBuffer(baos.toByteArray());
	}
}
